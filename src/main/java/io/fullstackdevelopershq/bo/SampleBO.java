package io.fullstackdevelopershq.bo;


public class SampleBO {
	
	private String productName;
	private AnotherBO anotherBO;
	
	
	public SampleBO() {
		System.out.println("Spring is using this constructor now...");
		// TODO Auto-generated constructor stub
	}


	public SampleBO(String productName, AnotherBO anotherBO) {
		this.productName = productName;
		this.anotherBO = anotherBO;
		 
	}
	


	 public AnotherBO getAnotherBO() {
		return anotherBO;
	}


	public void setAnotherBO(AnotherBO anotherBO) {
		this.anotherBO = anotherBO;
		System.out.println("This is setter injection for complex data types...objects");

	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
		System.out.println("This is setter injection...");
	}


	public void service(){
		System.out.println("Here some business logic is implemented in this method...");
	}
	public void init(){
		System.out.println("This is initialization code or spring initialization hooks...");
	}
}

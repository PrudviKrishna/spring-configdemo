package io.fullstackdevelopershq;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import io.fullstackdevelopershq.bo.SampleBO;

public class Application {
	
	private static SampleBO bo;

	public static void main(String[] args) {
		
    //SampleBO bo = new SampleBO();
	
	ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
	bo = context.getBean(SampleBO.class);
	SampleBO bo2 = context.getBean(SampleBO.class);
	
	System.out.println(bo==bo2);
//By default the scope of the bean is singleton.
	bo.service();
	
	}

}
